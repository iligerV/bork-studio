export enum RoutingPath {
    HOME = '/',
    ABOUT_US = 'AboutUs',
    PROJECTS = 'Projects',
    BLOG_PAGE = 'BlogPage',
    CAREER = 'Career',
    CONTACTS = 'Contacts',
}
