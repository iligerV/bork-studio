export enum NavigationItemsArticles {
    HOME = 'Home',
    ABOUT_US = 'About us',
    PROJECTS = 'Projects',
    BLOG_PAGE = 'Blog page',
    CAREER = 'Career',
    CONTACTS = 'Contacts',
}
