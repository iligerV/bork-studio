import * as React from 'react';
// Styles
import styles from './styles.scss';

const SecondBlock: React.FC = () => {
    return (
        <div className={styles.main_wrapper}>
            <div className={styles.aboutUs_link}>About us</div>
            <div className={styles.article_wrapper}>
                <div className={styles.article}>Unique</div>
                <div className={styles.article}>Stylish</div>
                <div className={styles.article}>Bright design</div>
            </div>
        </div>
    );
};

export default SecondBlock;
