import * as React from 'react';
// Components
import SecondBlock from './SecondBlock';
import ThirdBlock from './ThirdBlock';
// Styles
import styles from './styles.scss';
// Assets
import BORK_Icon from '../../../../../assets/BÖRK.svg';

const TopSection: React.FC = () => {
    return (
        <div className={styles.inner_wrapper}>
            <div className={styles.first_block}>
                <img className={styles.svg} src={BORK_Icon} alt={'BorkLogo'} />
            </div>
            <div className={styles.second_block}>
                <SecondBlock />
            </div>
            <div className={styles.third_block}>
                <ThirdBlock />
            </div>
            <div className={styles.fourth_block}>
                <div className={styles.contacts}>Contacts</div>
            </div>
        </div>
    );
};

export default TopSection;
