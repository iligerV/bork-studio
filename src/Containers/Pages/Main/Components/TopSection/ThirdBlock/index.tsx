import * as React from 'react';
import { Link } from 'react-router-dom';
// Styles
import styles from './styles.scss';
// Assets
import Icon_Menu from '../../../../../../assets/Icons/Icon_Menu.svg';

const ThirdBlock: React.FC = () => {
    return (
        <Link className={styles.link_to_menu} to="/menu">
            <img className={styles.icon_menu} src={Icon_Menu} alt={'Icon_Menu'} />
        </Link>
    );
};

export default ThirdBlock;
