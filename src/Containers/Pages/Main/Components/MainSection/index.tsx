import * as React from 'react';

const MainSection: React.FC = () => {
    return (
        <div
            style={{
                display: 'flex',
                height: '100%',
                minHeight: '376px',
                width: '1600px',
                margin: 'auto',
            }}
        >
            MainSection
        </div>
    );
};

export default MainSection;
