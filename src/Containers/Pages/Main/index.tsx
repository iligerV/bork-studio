import * as React from 'react';
// Components
import BtmSection from './Components/BtmSection';
import TopSection from './Components/TopSection';
import MainSection from './Components/MainSection';
// Assets
import Horizontal_Overlay from '../../../assets/horizontal_overlay.svg';

const MainPage: React.FC = () => {
    return (
        <>
            <div
                style={{
                    display: 'flex',
                    flexDirection: 'column',
                    flexGrow: 1,
                    background: `repeat-x center url(${Horizontal_Overlay})`,
                    borderBottom: '1px solid #cccccc',
                }}
            >
                <TopSection />
                <MainSection />
            </div>
            <BtmSection />
        </>
    );
};

export default MainPage;
