import * as React from 'react';
// Components
import BtmSection from './Components/BtmSection';
import MainSection from './Components/MainSection';
// Assets
import Horizontal_Overlay from '../../../assets/horizontal_overlay.svg';

const MenuPage: React.FC = () => {
    return (
        <>
            <div
                style={{
                    flexBasis: '100%',
                    background: `repeat-x center url(${Horizontal_Overlay})`,
                    borderBottom: '1px solid #cccccc',
                }}
            >
                <MainSection />
            </div>
            <BtmSection />
        </>
    );
};

export default MenuPage;
