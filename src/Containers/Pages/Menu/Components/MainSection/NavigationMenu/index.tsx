import * as React from 'react';
// Types
import { NavigationItemsArticles } from '../../../../../../Models/MenuPage';
import { RoutingPath } from '../../../../../../Models/Routing';
// Styles
import styles from './styles.scss';

interface OwnProps {
    setFocusedNavItem: (focusedItem: string) => void;
}

const navigationItems = [
    { article: NavigationItemsArticles.HOME, linkTo: RoutingPath.HOME },
    { article: NavigationItemsArticles.ABOUT_US, linkTo: RoutingPath.ABOUT_US },
    { article: NavigationItemsArticles.PROJECTS, linkTo: RoutingPath.PROJECTS },
    { article: NavigationItemsArticles.BLOG_PAGE, linkTo: RoutingPath.BLOG_PAGE },
    { article: NavigationItemsArticles.CAREER, linkTo: RoutingPath.CAREER },
    { article: NavigationItemsArticles.CONTACTS, linkTo: RoutingPath.CONTACTS },
];

interface NavigationItem {
    article: string;
    linkTo: string;
}

const NavigationMenu: React.FC<OwnProps> = ({ setFocusedNavItem }: OwnProps) => {
    const renderNavigationItem = ({ article, linkTo }: NavigationItem, i: number): JSX.Element => {
        return (
            <div
                key={i}
                className={styles.navigation_item}
                onMouseOver={() => setFocusedNavItem(linkTo)}
                onMouseLeave={() => setFocusedNavItem('')}
            >
                {article}
            </div>
        );
    };

    return (
        <div className={styles.navigation_wrapper}>{navigationItems.map(renderNavigationItem)}</div>
    );
};

export default React.memo(NavigationMenu);
