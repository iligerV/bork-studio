import React, { LegacyRef, useRef } from 'react';
// Styles
import styles from './styles.scss';
// Assets => MenuPresentation
import Home_SVG from '../../../../../../assets/Menu/Presentation/Home.svg';
import AboutUs_SVG from '../../../../../../assets/Menu/Presentation/AboutUs.svg';
import Projects_SVG from '../../../../../../assets/Menu/Presentation/Projects.svg';
import BlogPage_SVG from '../../../../../../assets/Menu/Presentation/BlogPage.svg';
import Career_SVG from '../../../../../../assets/Menu/Presentation/Career.svg';
import Contacts_SVG from '../../../../../../assets/Menu/Presentation/Contacts.svg';

interface OwnProps {
    focusedNavItem: string;
}

const PresentationSVGPathTo = {
    '/': Home_SVG,
    AboutUs: AboutUs_SVG,
    Projects: Projects_SVG,
    BlogPage: BlogPage_SVG,
    Career: Career_SVG,
    Contacts: Contacts_SVG,
};

const MovingLineArticle = {
    '/': 'Home',
    AboutUs: 'About us',
    Projects: 'Projects',
    BlogPage: 'Blog page',
    Career: 'Career',
    Contacts: 'Contacts',
};

const PagePresentationSection: React.FC<OwnProps> = ({ focusedNavItem }: OwnProps) => {
    const movingLineRef: LegacyRef<HTMLDivElement> = useRef();
    const windowWidth = window.innerWidth;
    const mainWrapperWidth = Number(
        document.getElementById('mainWrapper').style.minWidth.replace('px', '')
    );

    const countInitAnimationState = (): number => {
        const rightPadding: number = (windowWidth - mainWrapperWidth) / 2;
        return (mainWrapperWidth + rightPadding) * -1;
    };

    const countFinishAnimationState = (animationBlockWidth: number): number => {
        const leftPadding: number = (windowWidth - mainWrapperWidth) / 2;
        return leftPadding + animationBlockWidth;
    };

    const animateLineWrapper = () => {
        let animationPositionState = countInitAnimationState();

        return function animation() {
            const animationStep = 6;
            const animationBlockWidth = movingLineRef.current.clientWidth;

            animationPositionState += animationStep;
            movingLineRef.current.style.right = `${animationPositionState}px`;

            if (animationPositionState >= countFinishAnimationState(animationBlockWidth)) {
                animationPositionState = countInitAnimationState();
            }

            requestAnimationFrame(animation);
        };
    };

    React.useEffect(() => {
        requestAnimationFrame(animateLineWrapper());
    }, []);

    return (
        <div
            className={styles.svg_page_presentation}
            style={{
                background: `url(${PresentationSVGPathTo[focusedNavItem]}) center no-repeat`,
            }}
        >
            <div ref={movingLineRef} className={styles.moving_line}>
                {MovingLineArticle[focusedNavItem]}
            </div>
        </div>
    );
};

export default React.memo(PagePresentationSection);
