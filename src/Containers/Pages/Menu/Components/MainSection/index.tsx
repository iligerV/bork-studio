import * as React from 'react';
import { Link } from 'react-router-dom';
// Components
import NavigationMenu from './NavigationMenu';
import PagePresentationSection from './PagePresentationSection';
// Styles
import styles from './styles.scss';
// Assets => Logo/Icon
import BORK_Icon from '../../../../../assets/BÖRK.svg';
import Icon_Menu_Close from '../../../../../assets/Icons/Icon_Menu_Close.svg';

const MainSection: React.FC = () => {
    const [focusedNavItem, setFocusedNavItem] = React.useState<string>('');

    return (
        <div className={styles.main_wrapper}>
            <div className={styles.first_section}>
                <div className={styles.svg_logo_container}>
                    <img className={styles.svg_logo} src={BORK_Icon} alt={'BorkLogo'} />
                </div>
                <PagePresentationSection focusedNavItem={focusedNavItem} />
            </div>
            <div className={styles.second_section}>
                <div className={styles.navigation_wrapper}>
                    <Link className={styles.link} to="/">
                        <img
                            className={styles.close_menu}
                            src={Icon_Menu_Close}
                            alt={'Icon_Menu_Close'}
                        />
                    </Link>
                    <NavigationMenu setFocusedNavItem={setFocusedNavItem} />
                </div>
            </div>
        </div>
    );
};

export default React.memo(MainSection);
