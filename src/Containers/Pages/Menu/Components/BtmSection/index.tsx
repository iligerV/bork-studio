import * as React from 'react';
// Styles
import styles from './styles.scss';

const BtmSection: React.FC = () => {
    return (
        <div className={styles.btm_section_wrapper}>
            <div className={styles.links_wrapper}>
                <div className={styles.link}>facebook</div>
                <div className={styles.link}>instagram</div>
            </div>
            <div className={styles.phone_number}>00 – 7 – (095) – 1234567</div>
        </div>
    );
};

export default BtmSection;
