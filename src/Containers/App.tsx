import * as React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
// Components
import MainPage from './Pages/Main';
import MenuPage from './Pages/Menu';
// Assets
import Vertical_Overlay from '../assets/vertical_overlay.svg';

const App: React.FC = () => {
    return (
        <div
            id={'mainWrapper'}
            style={{
                display: 'flex',
                flexDirection: 'column',
                height: '100vh',
                minHeight: '872px',
                minWidth: '1600px',
                overflow: 'hidden',
                background: `repeat-y center url(${Vertical_Overlay})`,
            }}
        >
            <Router>
                <Switch>
                    <Route exact path="/">
                        <MainPage />
                    </Route>
                    <Route path="/menu">
                        <MenuPage />
                    </Route>
                    <Route path="/smt_more">SMT_MORE</Route>
                </Switch>
            </Router>
        </div>
    );
};

export default App;
