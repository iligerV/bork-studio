import { render } from 'react-dom';
import * as React from 'react';
// Components
import App from './Containers/App';
// styles
import './Common/DefaultCss/styles.css';
import './Common/RootCss/styles.scss';

render(<App />, document.getElementById('root-app'));
