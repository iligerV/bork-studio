const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserWebpackPlugin = require('terser-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const path = require('path');

const { NODE_ENV } = process.env;
const isDevelopment = NODE_ENV === 'development';
const isProduction = !isDevelopment;

const getBabelOptions = (presets = []) => ({
    presets: ['@babel/preset-env', ...presets],
});

const getOptimizationOptions = () => {
    const options = {
        splitChunks: {
            chunks: 'all',
        },
    };

    if (isProduction) {
        options.minimizer = [new TerserWebpackPlugin(), new OptimizeCssAssetsPlugin()];
    }

    return options;
};

const getStylesLoader = (loaders = []) => [
    {
        loader: MiniCssExtractPlugin.loader,
        options: {
            esModule: true,
            hmr: isDevelopment,
        },
    },
    {
        loader: 'css-loader',
        options: {
            modules: {
                mode: 'local',
                exportGlobals: true,
                localIdentName: '[name].[local].[hash:base64:5]',
                context: path.resolve(__dirname, 'src'),
                hashPrefix: 'hash',
            },
            localsConvention: 'camelCase',
            esModule: true,
        },
    },
    ...loaders,
];

module.exports = {
    mode: NODE_ENV,
    entry: './src/index.tsx',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].[hash].js',
    },
    devtool: isDevelopment ? 'source-map' : '',
    optimization: getOptimizationOptions(),
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json'],
    },
    module: {
        rules: [
            {
                test: /\.js/,
                exclude: /node_modules/,
                loader: {
                    loader: 'babel-loader',
                    options: getBabelOptions(),
                },
            },
            {
                test: /\.ts/,
                exclude: /node_modules/,
                loader: {
                    loader: 'babel-loader',
                    options: getBabelOptions(['@babel/preset-typescript']),
                },
            },
            {
                test: /\.tsx/,
                exclude: /node_modules/,
                loader: {
                    loader: 'babel-loader',
                    options: getBabelOptions(['@babel/preset-typescript', '@babel/preset-react']),
                },
            },
            {
                test: /\.css$/,
                use: getStylesLoader(),
            },
            {
                test: /\.s[ac]ss$/,
                use: getStylesLoader(['sass-loader']),
            },
            {
                test: /\.(png|jpe?g|gif|svg|ttf|woff)$/i,
                loader: 'file-loader',
                options: {
                    name: '[name].[hash].[ext]',
                    outputPath: 'assets',
                },
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: './src/index.html',
            favicon: './src/assets/Favicon.svg',
        }),
        new MiniCssExtractPlugin({
            filename: '[name].[contenthash].css',
            chunkFilename: '[id].[contenthash].css',
        }),
        new ForkTsCheckerWebpackPlugin({
            typescript: {
                mode: 'write-references',
            },
            eslint: {
                files: 'src/**/*.{ts,tsx,js,jsx}',
            },
        }),
    ],
    devServer: {
        compress: true,
        host: '0.0.0.0',
        useLocalIp: true,
        port: 3030,
        hotOnly: isDevelopment,
        clientLogLevel: 'warning',
        overlay: true,
    },
};
